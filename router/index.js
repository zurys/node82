import express from "express";
import json from 'body-parser';
import alumnosoBd from "../models/alumnos.js";

export const router = express.Router();

export default { router };
//declarar primer ruta por omision

router.get('/', (req, res) => {

    res.render('index', { titulo: "MIS PRACTICAS", nombre: "IRVING QUINTERO" })
});

router.get('/tabla', (req, res) => {
    //parametros
    const params = {
        numero: req.query.numero
    }
    res.render('tabla', params);
});



router.post('/tabla', (req, res) => {
    //parametros
    const params = {
        numero: req.body.numero
    }
    res.render('tabla', params);
});

router.get('/cotizacion', (req, res) => {

    const valor = parseFloat(req.query.valor);
    const pinicial = parseFloat(req.query.pinicial);
    const plazos = parseInt(req.query.plazos);

    const valorFinal = valor - (pinicial * valor / 100) ;
    const cuotaMensual = valorFinal / plazos;

    const params = {
        valor: valor,
        pinicial: pinicial,
        plazos: plazos,
        valorFinal: valorFinal,
        cuotaMensual: cuotaMensual
    };

    res.render('cotizacion', params);
});

router.post('/cotizacion', (req, res) => {
    const valor = parseFloat(req.body.valor);
    const pinicial = parseFloat(req.body.pinicial);
    const plazos = parseInt(req.body.plazos);

    const valorFinal = valor - (pinicial * valor / 100) ;
    const cuotaMensual = valorFinal / plazos;

    const params = {
        valor: valor,
        pinicial: pinicial,
        plazos: plazos,
        valorFinal: valorFinal,
        cuotaMensual: cuotaMensual
    };

    res.render('cotizacion', params);
});

let rows;
router.get('/alumnos', async(req, res) => {

    rows = await alumnosoBd.mostrarTodos();
    res.render('alumnos', {reg:rows});
});


let params;
router.post('/alumnos', async(req, res) => {
        try {
            params = {
                matricula: req.body.matricula,
                nombre: req.body.nombre,
                domicilio: req.body.domicilio,
                sexo: req.body.sexo,
                especialidad: req.body.especialidad
            }
            const res = await alumnosoBd.insertar(params);
        } catch (error) {
            console.error(error);
            res.status(400).send(`sucedio un error ${error}`);
        }
    
        rows = await alumnosoBd.mostrarTodos();    
        res.render(`alumnos`, {reg:rows});

    });



